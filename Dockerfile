FROM nimmis/java-centos

EXPOSE 8222

ADD target/gateway-1.0-SNAPSHOT.jar   /opt/gateway.jar

ENTRYPOINT ["sh","-c","java -Duser.timezone=GMT+08 -jar /opt/gateway.jar"]
